package com.khusnullin.headHunterParser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeadHunterParserApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeadHunterParserApplication.class, args);
    }

}
