package com.khusnullin.headHunterParser.controller;

import com.khusnullin.headHunterParser.model.Vacancy;
import com.khusnullin.headHunterParser.service.UtilityService;
import com.khusnullin.headHunterParser.service.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TestController {

    private final VacancyService vacancyService;
    private final UtilityService utilityService;

    @Autowired
    public TestController(VacancyService vacancyService, UtilityService utilityService) {
        this.vacancyService = vacancyService;
        this.utilityService = utilityService;
    }

    @GetMapping("/link")
    public String handleLink(@RequestParam("link") String link) {
        System.out.println("LINK RECEIVED: " + link);
        System.out.println(vacancyService.getVacancyIdFromJson(link));
        return String.valueOf(vacancyService.getVacancyIdFromJson(link));
    }

    @GetMapping("/test/area")
    public ResponseEntity<String> getIdForArea() {
        return utilityService.findAreaIdByName("москВа");
    }

    //TODO пагинация, поиск
    @GetMapping("/vacancies/region")
    public ResponseEntity<List<Vacancy>> getVacanciesByRegion(
            @RequestParam("region") int region,
            @RequestParam(defaultValue = "1") int page) {

        List<Vacancy> vacancies = vacancyService.findVacanciesByRegion(region, page);
        return ResponseEntity.ok(vacancies);
    }

}
