package com.khusnullin.headHunterParser.controller;

import com.khusnullin.headHunterParser.model.Employer;
import com.khusnullin.headHunterParser.model.Vacancy;
import com.khusnullin.headHunterParser.service.EmployerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/api/employers")
public class EmployerController {

    private final EmployerService employerService;

    public EmployerController(EmployerService employerService) {
        this.employerService = employerService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employer> getEmployerInfo(@PathVariable("id") int id) {
        ResponseEntity<Employer> employerResponse = employerService.getEmployer(id);
        if (employerResponse.getStatusCode() == HttpStatus.OK) {
            return employerResponse;
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //TODO тут должен быть POST или совсем не нужен метод?
    @PostMapping("/save/{id}")
    public Employer saveNewEmployer(@PathVariable("id") int id) {
        return employerService.getOrCreateEmployer(id);

    }

}
