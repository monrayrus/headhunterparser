package com.khusnullin.headHunterParser.controller;

import com.khusnullin.headHunterParser.model.Vacancy;
import com.khusnullin.headHunterParser.service.EmployerService;
import com.khusnullin.headHunterParser.service.UtilityService;
import com.khusnullin.headHunterParser.service.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/api/vacancies")
public class VacancyController {

    private final VacancyService vacancyService;
    private final UtilityService utilityService;

    @Autowired
    public VacancyController(VacancyService vacancyService, UtilityService utilityService) {
        this.vacancyService = vacancyService;
        this.utilityService = utilityService;
    }
    @PostMapping("/fetch/link")
    public ResponseEntity<Vacancy> fetchVacancyFromUrl(@RequestParam("link") String link) {
        return vacancyService.fetchVacancyJson(link);
    }

    //TODO возможно стоит объединить в один метод парсинг и выдачу описания вакансии
    @GetMapping("/{id}/description")
    public String getDescriptionFromVacancyAndEmployer(@PathVariable("id") int vacancyId) {
        return utilityService.getWholeDescription(vacancyId);
    }

    @GetMapping("/from/{id}")
    public Set<Vacancy> getListOfIDs(@PathVariable("id") int id) {
        return vacancyService.getSetOfVacanciesFromEmployerId(id);
    }

}
