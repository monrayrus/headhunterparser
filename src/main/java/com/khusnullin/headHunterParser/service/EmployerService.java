package com.khusnullin.headHunterParser.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.khusnullin.headHunterParser.model.Employer;
import com.khusnullin.headHunterParser.model.Vacancy;
import com.khusnullin.headHunterParser.repository.EmployerRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

@Service
public class EmployerService {

    private static final Logger log = LoggerFactory.getLogger(VacancyService.class);
    private final EmployerRepository employerRepository;
    private final ObjectMapper objectMapper;

    public EmployerService(EmployerRepository employerRepository, ObjectMapper objectMapper) {
        this.employerRepository = employerRepository;
        this.objectMapper = objectMapper;
    }

    public Employer getOrCreateEmployer(int employerId) {
        //TODO не уверен что это нужно, проверить!
        if (employerId == 0) {
            log.info("Saved as default employer");
            return createDefaultEmployer();
        }
        Optional<Employer> optionalEmployer = employerRepository.findById(employerId);
        if (optionalEmployer.isPresent()) {
            return optionalEmployer.get();
        } else {
            String url = "https://api.hh.ru/employers/" + employerId;
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
            try {
                Employer employer = objectMapper.readValue(response.getBody(), Employer.class);
                employer.setDescription(convertDescriptionToText(employer.getDescription()));
                employerRepository.save(employer);
                log.info("employer saved with id: " + employerId);
                return employer;
            } catch (JsonProcessingException e) {
                throw new RuntimeException("Failed to save employer ", e);
            }

        }
    }

    private Employer createDefaultEmployer() {
        return employerRepository.findById(0)
                .orElseGet(() -> {
                    Employer defaultEmployer = new Employer();
                    defaultEmployer.setId(0);
                    defaultEmployer.setName("БЕЗ ИМЕНИ");
                    defaultEmployer.setDescription("БЕЗ ИНФОРМАЦИИ");
                    employerRepository.save(defaultEmployer);
                    return defaultEmployer;
                });
    }


    public ResponseEntity<Employer> getEmployer(int id) {
        String url = "https://api.hh.ru/employers/" + id;
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
            Employer employer = objectMapper.readValue(response.getBody(), Employer.class);
            return new ResponseEntity<>(employer, HttpStatus.OK);
        } catch (HttpClientErrorException e) {
            log.info("Employer not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


    public String convertDescriptionToText(String html) {
        Document document = Jsoup.parse(html);
        return document.body().text();
    }

    public String getEmployerDescription(int employerId) {
        Optional<Employer> optionalEmployer = employerRepository.findById(employerId);
        if (optionalEmployer.isPresent()) {
            return optionalEmployer.get().getDescription();
        } else {
            throw new NoSuchElementException("No employer found with id: " + employerId);
        }
    }

}
