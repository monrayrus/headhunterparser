package com.khusnullin.headHunterParser.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.khusnullin.headHunterParser.configuration.ApiLinksConfig;
import com.khusnullin.headHunterParser.model.Employer;
import com.khusnullin.headHunterParser.model.Vacancy;
import com.khusnullin.headHunterParser.repository.EmployerRepository;
import com.khusnullin.headHunterParser.repository.VacancyRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class VacancyService {
    private static final Logger log = LoggerFactory.getLogger(VacancyService.class);
    private final VacancyRepository vacancyRepository;
    private final ObjectMapper objectMapper;
    private final EmployerService employerService;
    private final EmployerRepository employerRepository;


    @Autowired
    private Environment env;
    @Autowired
    private ApiLinksConfig apiLinksConfig;

    @Autowired
    public VacancyService(VacancyRepository vacancyRepository, ObjectMapper objectMapper, EmployerService employerService, EmployerRepository employerRepository) {
        this.vacancyRepository = vacancyRepository;
        this.objectMapper = objectMapper;
        this.employerService = employerService;
        this.employerRepository = employerRepository;
    }


    public ResponseEntity<Vacancy> fetchVacancyJson(String url) {
        int vacancyId = getVacancyIdFromJson(url);
        String apiUrl = getApiUrl(url) + getVacancyIdFromJson(url);
        log.info(apiUrl);
        RestTemplate restTemplate = new RestTemplate();
        Optional<Vacancy> existingVacancy = vacancyRepository.findByIdFromJson(vacancyId);
        if (existingVacancy.isPresent()) {
            return ResponseEntity.ok(existingVacancy.get());
        }
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(apiUrl, String.class);
            log.info(String.valueOf(response));
            Vacancy vacancy = objectMapper.readValue(response.getBody(), Vacancy.class);
            vacancy.setIdFromJson(vacancyId);
            vacancy.setUrlToSource(url);
            vacancy.setDescription(convertDescriptionToText(vacancy.getDescription()));
            Employer employer = employerService.getOrCreateEmployer(extractEmployerId(response));
            vacancy.setEmployer(employer);
            vacancyRepository.save(vacancy);
            log.info("Saved vacancy with ID: " + vacancy.getId());
            return ResponseEntity.ok(vacancy);
        } catch (HttpClientErrorException.NotFound e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            throw new RuntimeException("Failed to parse and save vacancy", e);
        }
    }


    public String getVacancyDescription(int vacancyId) {
        Optional<Vacancy> optionalVacancy = vacancyRepository.findById(vacancyId);
        if (optionalVacancy.isPresent()) {
            return optionalVacancy.get().getDescription();
        } else {
            throw new NoSuchElementException("No vacancy found with id: " + vacancyId);
        }
    }

    public String convertDescriptionToText(String html) {
        Document document = Jsoup.parse(html);
        return document.body().text();
    }


    public String getApiUrl(String url) {
        return apiLinksConfig.getApiUrl(getDomain(url));
    }

    public String getDomain(String url) {
        try {
            URL parsedUrl = new URL(url);
            String host = parsedUrl.getHost();
            String[] parts = host.split("\\.");
            if (parts.length > 2) {
                return parts[parts.length - 2] + "." + parts[parts.length - 1]; // Возвращает: xx.xxx если есть поддомен (например: msk.hh.ru)
            } else {
                return (parts[0] + "." + parts[1]); // Возвращает: xx.xxx если нет поддомена (например: hh.ru)
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public int getVacancyIdFromJson(String url) {
        Pattern pattern = Pattern.compile("/vacancy/(card/id)?(\\d+)");
        Matcher matcher = pattern.matcher(url);
        if (matcher.find()) {
            return Integer.parseInt(matcher.group(2));
        }
        return 0;
    }

    public int extractEmployerId(ResponseEntity<String> response) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(response.getBody());
            return node.get("employer").get("id").asInt();
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to parse JSON", e);
        }
    }



    public List<Vacancy> findVacanciesByRegion(int region, int page) {
        RestTemplate restTemplate = new RestTemplate();
        String url = buildUrl(region, page);
        ResponseEntity<List<Vacancy>> response = restTemplate.exchange(
                url, HttpMethod.GET, null, new ParameterizedTypeReference<List<Vacancy>>() {}
        );
        return response.getBody();
    }

    public String buildUrl(int region, int page) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("https://api.hh.ru")
                .pathSegment("vacancies")
                .queryParam("area", region)
                .queryParam("page", page)
                .queryParam("per_page", 10);
        return builder.build().encode().toUriString();
    }

    public Set<Vacancy> getSetOfVacanciesFromEmployerId(int employerId) {
        Optional<Employer> employer = employerRepository.findById(employerId);
        Set<Vacancy> vacancies = new HashSet<>();
        if (employer.isPresent()) {
            vacancies = employer.get().getVacancies();
        }
        return vacancies;
    }


}


