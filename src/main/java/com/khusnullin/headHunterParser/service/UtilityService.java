package com.khusnullin.headHunterParser.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.khusnullin.headHunterParser.model.Vacancy;
import com.khusnullin.headHunterParser.repository.VacancyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UtilityService {

    private final EmployerService employerService;
    private final VacancyService vacancyService;
    private final VacancyRepository vacancyRepository;
    private final ObjectMapper objectMapper;

    private static final Logger log = LoggerFactory.getLogger(VacancyService.class);


    public UtilityService(EmployerService employerService, VacancyService vacancyService, VacancyRepository vacancyRepository, ObjectMapper objectMapper) {
        this.employerService = employerService;
        this.vacancyService = vacancyService;
        this.vacancyRepository = vacancyRepository;
        this.objectMapper = objectMapper;
    }


    public ResponseEntity<String> findAreaIdByName(String targetName) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<String> result = restTemplate.exchange(
                "https://api.hh.ru/areas", HttpMethod.GET, entity, String.class);
        List<Map<String, Object>> list = null;
        try {
            list = objectMapper.readValue(result.getBody(), new TypeReference<List<Map<String, Object>>>() {
            });
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        Optional<String> areaId = findAreaIdInList(list, capitalizeFirstLetter(targetName));
        return areaId.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    private Optional<String> findAreaIdInList(List<Map<String, Object>> list, String targetName) {
        for (Map<String, Object> map : list) {
            if (map.containsKey("name") && map.get("name").equals(targetName)) {
                return Optional.of((String) map.get("id"));
            }
            if (map.containsKey("areas")) {
                List<Map<String, Object>> subList = (List<Map<String, Object>>) map.get("areas");
                Optional<String> id = findAreaIdInList(subList, targetName);
                if (id.isPresent()) {
                    return id;
                }
            }
        }
        return Optional.empty();
    }


    public String getWholeDescription(int vacancyId) {
        String wholeDescription = "NO_DESCRIPTION";
        if (vacancyRepository.findById(vacancyId).isPresent()) {
            Vacancy vacancy = vacancyRepository.findById(vacancyId).get();
            String employerDescription = "EMPLOYER DESCRIPTION: \n" + employerService.getEmployerDescription(vacancy.getEmployerId());
            String vacancyDescription = "VACANCY DESCRIPTION: \n" + vacancyService.getVacancyDescription(vacancyId);
            wholeDescription = employerDescription + "\n" + vacancyDescription;
        }
        return wholeDescription;
    }


    public static String capitalizeFirstLetter(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }
        char firstChar = Character.toUpperCase(text.charAt(0));
        String remainingChars = text.substring(1).toLowerCase();
        return firstChar + remainingChars;
    }
}
