package com.khusnullin.headHunterParser.repository;

import com.khusnullin.headHunterParser.model.Employer;
import com.khusnullin.headHunterParser.model.Vacancy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VacancyRepository extends JpaRepository<Vacancy, Integer> {
    Optional<Vacancy> findByIdFromJson(int id);
}
