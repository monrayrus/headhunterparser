package com.khusnullin.headHunterParser.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:api-links.properties")
public class ApiLinksConfig {
    @Autowired
    private Environment env;
    public String getApiUrl(String domain) {
        return env.getProperty(domain);
    }
}