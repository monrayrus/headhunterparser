package com.khusnullin.headHunterParser.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
public class Vacancy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    @Column(columnDefinition = "TEXT")
    private String description;
    private int idFromJson;

    private String urlToSource;

    @ManyToOne
    @JoinColumn(name = "employer_id", nullable = true)
    @JsonIgnore
    private Employer employer;

    public Vacancy() {
    }

    public Vacancy(String name, String description, int idFromJson, String urlToSource) {
        this.name = name;
        this.description = description;
        this.idFromJson = idFromJson;
        this.urlToSource = urlToSource;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String title) {
        this.name = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdFromJson() {
        return idFromJson;
    }

    public void setIdFromJson(int idFromJson) {
        this.idFromJson = idFromJson;
    }

    public String getUrlToSource() {
        return urlToSource;
    }

    public void setUrlToSource(String urlToSource) {
        this.urlToSource = urlToSource;
    }

    public int getEmployerId() {
        return this.employer != null ? this.employer.getId() : 0;
    }

    public Employer getEmployer() {
        return this.employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }
}
