package com.khusnullin.headHunterParser;

import com.khusnullin.headHunterParser.controller.VacancyController;
import com.khusnullin.headHunterParser.service.VacancyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(VacancyController.class)
public class VacancyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VacancyService vacancyService;

    @Test
    public void testFetchVacancyFromUrl() throws Exception {
        String link = "http://example.com/vacancy/1";
        mockMvc.perform(get("/api/fetch/link?link=" + link))
                .andExpect(status().isOk());
    }
}